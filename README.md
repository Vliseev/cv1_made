#### Выполнить комманды
```
python script/convert_data.py --data data

python script/hack_train.py --name resnet50w \
-lr 1e-1 --data data -e 20 --gpu --batch-size=512

python script/cont_train_train.py --name resnet50w \
  -lr 1e-2 --data data -e 12 --gpu --batch-size=512

python script/cont_train_train.py --name resnet50w \
 -lr 1e-2 --data data -e 12 --gpu --batch-size=512
```

![](submit.png)