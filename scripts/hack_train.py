import gc
import pickle
import sys
from argparse import ArgumentParser

import numpy as np
import torch
import tqdm
from torch.nn import functional as fnn
from torch.optim.adamw import AdamW
from torch.optim.lr_scheduler import StepLR
from torch.utils import data
from torch.utils.tensorboard import SummaryWriter
from torchvision import transforms

from hack_utils import (CROP_SIZE, NUM_PTS, CropCenter,
                        RotateTransform, ScaleMinSideToSize,
                        TransformByKeys, ValDataset,
                        create_submission, restore_landmarks_batch)
from nn_factory import (DensnetBuilder, ObjectFactory, ResnetBuilder,
                        ResnextBuilder)

torch.backends.cudnn.deterministic = True
torch.backends.cudnn.benchmark = False


def parse_arguments():
    parser = ArgumentParser(__doc__)
    parser.add_argument("--name", "-n", help="Experiment name (for saving checkpoints and submits).",
                        default="baseline")
    parser.add_argument("--data", "-d", help="Path to dir with target images & landmarks.", default=None)
    parser.add_argument("--batch-size", "-b", default=512, type=int)  # 512 is OK for resnet18 finetune @ 6Gb of VRAM
    parser.add_argument("--epochs", "-e", default=1, type=int)
    parser.add_argument("--minibatch-val", "-m", default=None)
    parser.add_argument("--learning-rate", "-lr", default=1e-3, type=float)
    parser.add_argument("--gpu", action="store_true")
    return parser.parse_args()


def train(model, loader, val_loader, loss_fn, optimizer, epoch, minibatch_val, device, writer):
    model.train()
    train_loss = []
    for i, batch in tqdm.tqdm(enumerate(loader), total=len(loader), desc="training..."):
        images = batch["image"].to(device)  # B x 3 x CROP_SIZE x CROP_SIZE
        landmarks = batch["landmarks"]  # B x (2 * NUM_PTS)

        pred_landmarks = model(images).cpu()  # B x (2 * NUM_PTS)
        loss = loss_fn(pred_landmarks, landmarks, reduction="mean")
        train_loss.append(loss.item())

        optimizer.zero_grad()
        loss.backward()
        optimizer.step()

        if not (minibatch_val is None) and (
                i % int(minibatch_val) == int(minibatch_val) - 1):  # every 1000 mini-batches...

            # ...log the running loss
            writer.add_scalar('training loss',
                              train_loss[-1],
                              i + epoch * len(loader))
            val_loss = validate(model, val_loader, loss_fn, device)
            writer.add_scalar('val loss',
                              val_loss,
                              i + epoch * len(loader))
            print(f"train_loss={train_loss[-1]} val_loss={val_loss}")
            model.train()

            # ...log a Matplotlib Figure showing the model's predictions on a
            # random mini-batch
            running_loss = 0.0

    return np.mean(train_loss)


def validate(model, loader, loss_fn, device):
    model.eval()
    val_loss = []
    for batch in tqdm.tqdm(loader, total=len(loader), desc="validation..."):
        images = batch["image"].to(device)
        landmarks = batch["landmarks"]

        with torch.no_grad():
            pred_landmarks = model(images).cpu()
        loss = loss_fn(pred_landmarks, landmarks, reduction="mean")
        val_loss.append(loss.item())

    return np.mean(val_loss)


def predict(model, loader, device):
    model.eval()
    predictions = np.zeros((len(loader.dataset), NUM_PTS, 2))
    for i, batch in enumerate(tqdm.tqdm(loader, total=len(loader), desc="test prediction...")):
        images = batch["image"].to(device)

        with torch.no_grad():
            pred_landmarks = model(images).cpu()
        pred_landmarks = pred_landmarks.numpy().reshape((len(pred_landmarks), NUM_PTS, 2))  # B x NUM_PTS x 2

        fs = batch["scale_coef"].numpy()  # B
        margins_x = batch["crop_margin_x"].numpy()  # B
        margins_y = batch["crop_margin_y"].numpy()  # B
        prediction = restore_landmarks_batch(pred_landmarks, fs, margins_x, margins_y)  # B x NUM_PTS x 2
        predictions[i * loader.batch_size: (i + 1) * loader.batch_size] = prediction

    return predictions


def init_factory(device):
    factory = ObjectFactory()
    factory.register_builder("densenet-161", DensnetBuilder(NUM_PTS, mod_name="161", device=device))
    factory.register_builder("densenet-201", DensnetBuilder(NUM_PTS, mod_name="201", device=device))
    factory.register_builder("resnext-50", ResnextBuilder(NUM_PTS, mod_name="50", device=device))
    factory.register_builder("resnext-101", ResnextBuilder(NUM_PTS, mod_name="101", device=device))
    factory.register_builder("resnet50w", ResnetBuilder(NUM_PTS, mod_name="50w", device=device))
    return factory


def main(args):
    # 1. prepare data & models
    train_transforms = transforms.Compose([
        ScaleMinSideToSize((CROP_SIZE, CROP_SIZE)),
        CropCenter(CROP_SIZE),
        RotateTransform(p=0.5, degree=30),
        TransformByKeys(transforms.ToTensor(), ("image",)),
        TransformByKeys(transforms.Normalize(mean=[0.5, 0.5, 0.5], std=[0.5, 0.5, 0.5]), ("image",)),
    ])

    val_transforms = transforms.Compose([
        ScaleMinSideToSize((CROP_SIZE, CROP_SIZE)),
        CropCenter(CROP_SIZE),
        TransformByKeys(transforms.ToPILImage(), ("image",)),
        TransformByKeys(transforms.ToTensor(), ("image",)),
        TransformByKeys(transforms.Normalize(mean=[0.5, 0.5, 0.5], std=[0.5, 0.5, 0.5]), ("image",)),
    ])

    # Create writer
    writer = SummaryWriter('runs/writer')

    print("Reading data train...")
    # train_dataset = ThousandLandmarksDataset(os.path.join(args.data, 'train'), train_transforms, split="train")
    train_dataset = ValDataset(args.data, train_transforms, split="train")
    train_dataloader = data.DataLoader(train_dataset, batch_size=args.batch_size, num_workers=4, pin_memory=True,
                                       shuffle=True, drop_last=True)

    gc.collect()
    print("Reading data val...")
    # val_dataset = ThousandLandmarksDataset(os.path.join(args.data, 'train'), train_transforms, split="val")
    val_dataset = ValDataset(args.data, val_transforms, split="val")
    val_dataloader = data.DataLoader(val_dataset, batch_size=args.batch_size, num_workers=4, pin_memory=True,
                                     shuffle=False, drop_last=False)
    gc.collect()
    print("Creating model...")
    device = torch.device("cuda: 0") if args.gpu else torch.device("cpu")
    nn_factory = init_factory(device)
    model = nn_factory.create('resnet50w')

    optimizer = AdamW(model.parameters(), lr=args.learning_rate, amsgrad=True)
    scheduler = StepLR(optimizer=optimizer, step_size=2,
                       gamma=0.3)  # CosineAnnealingLR(optimizer, eta_min=1e-4, T_max=3) #StepLR
    loss_fn = fnn.mse_loss

    # 2. train & validate
    print("Ready for training...")
    best_val_loss = np.inf
    for epoch in range(args.epochs):
        train_loss = train(model,
                           train_dataloader,
                           val_dataloader,
                           loss_fn,
                           optimizer,
                           minibatch_val=args.minibatch_val,
                           epoch=epoch,
                           device=device,
                           writer=writer)

        val_loss = validate(model, val_dataloader, loss_fn, device=device)
        writer.add_scalar('training loss',
                          train_loss,
                          (epoch + 1) * len(train_dataloader))
        writer.add_scalar('val loss',
                          val_loss,
                          (epoch + 1) * len(train_dataloader))

        print("Epoch #{:2}:\ttrain loss: {:5.2}\tval loss: {:5.2}".format(epoch, train_loss, val_loss))
        if val_loss < best_val_loss:
            best_val_loss = val_loss
            with open(f"{args.name}_best.pth", "wb") as fp:
                torch.save(model.state_dict(), fp)
        scheduler.step()

    # 3. predict

    # test_dataset = ThousandLandmarksDataset(os.path.join(args.data, 'test'), train_transforms, split="test")
    test_dataset = ValDataset(args.data, val_transforms, split="test")
    test_dataloader = data.DataLoader(test_dataset, batch_size=args.batch_size, num_workers=4, pin_memory=True,
                                      shuffle=False, drop_last=False)

    with open(f"{args.name}_best.pth", "rb") as fp:
        best_state_dict = torch.load(fp, map_location="cpu")
        model.load_state_dict(best_state_dict)

    test_predictions = predict(model, test_dataloader, device)
    with open(f"{args.name}_test_predictions.pkl", "wb") as fp:
        pickle.dump({"image_names": test_dataset.image_names,
                     "landmarks": test_predictions}, fp)

    create_submission(args.data, test_predictions, f"{args.name}_submit.csv")


if __name__ == '__main__':
    args = parse_arguments()
    sys.exit(main(args))
