from torch import nn
from torchvision import models


class ObjectFactory:
    def __init__(self):
        self._builders = {}

    def register_builder(self, key, builder):
        self._builders[key] = builder

    def create(self, key, **kwargs):
        builder = self._builders.get(key)
        if not builder:
            raise ValueError(key)
        return builder(**kwargs)


class ResnetBuilder:
    def __init__(self, num_pts, mod_name="50w", device='cpu'):
        self._instance = None
        self.num_pts = num_pts
        self.device = device
        self.mod_name = mod_name
        self.models_dict = {"50": models.resnet50,
                            "50w": models.wide_resnet50_2, }

    def __call__(self):
        if not self._instance:
            model = self.models_dict[self.mod_name](pretrained=True)
            model.fc = nn.Linear(model.fc.in_features, 2 * self.num_pts, bias=True)
            model.fc.requires_grad_(True)
            model.to(self.device)
            self._instance = model

        return self._instance


class ResnextBuilder:
    def __init__(self, num_pts, mod_name="50", device='cpu'):
        self._instance = None
        self.num_pts = num_pts
        self.device = device
        self.mod_name = mod_name
        self.models_dict = {"50": models.resnext50_32x4d,
                            "101": models.resnext101_32x8d
                            }

    def __call__(self):
        if not self._instance:
            model = self.models_dict[self.mod_name](pretrained=True)
            model.fc = nn.Linear(model.fc.in_features, 2 * self.num_pts, bias=True)
            model.to(self.device)
            model.fc.requires_grad_(True)

            self._instance = model

        return self._instance


class DensnetBuilder:
    def __init__(self, num_pts, mod_name="161", device='cpu'):
        self._instance = None
        self.num_pts = num_pts
        self.device = device
        self.mod_name = mod_name
        self.models_dict = {"161": models.densenet161,
                            "169": models.densenet169,
                            "201": models.densenet201}

    def __call__(self):
        if not self._instance:
            model = self.models_dict[self.mod_name](pretrained=True)
            model.classifier = nn.Linear(model.classifier.in_features, 2 * self.num_pts, bias=True)
            model.to(self.device)

            for p in model.parameters():
                p.requires_grad = False

            model.features.denseblock2.requires_grad_(True)
            model.features.denseblock3.requires_grad_(True)
            model.features.denseblock4.requires_grad_(True)
            model.features.norm5.requires_grad_(True)
            model.classifier.requires_grad_(True)

            self._instance = model

        return self._instance
