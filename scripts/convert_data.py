import logging
import os
from argparse import ArgumentParser

import numpy as np
import pandas as pd
import tqdm

from hack_utils import TRAIN_SIZE

CHINK_SIZE = 10000


def convert_data(root, split="train"):
    landmark_file_name = os.path.join(root, 'landmarks.csv') if split is not "test" \
        else os.path.join(root, "test_points.csv")

    col_names = pd.read_csv(landmark_file_name, nrows=0, sep='\t').columns
    n_points = len(col_names) - 1

    with open(landmark_file_name, "rt") as fp:
        num_lines = sum(1 for line in fp)
    num_lines -= 1  # header

    matrix_size = {
        "train": int(TRAIN_SIZE * num_lines),
        "val": num_lines - int(TRAIN_SIZE * num_lines),
        "test": num_lines
    }

    split_idxs = {"train": (0, int(TRAIN_SIZE * num_lines)),
                  "val": (int(TRAIN_SIZE * num_lines), num_lines),
                  "test": (0, num_lines)}

    idxs = split_idxs[split]

    if split in ("train", "val"):
        chunks = pd.read_csv(landmark_file_name, skiprows=idxs[0], nrows=idxs[1], delimiter='\t',
                             dtype={name: np.uint16 for name in col_names[1:]},
                             chunksize=CHINK_SIZE)
    else:
        chunks = pd.read_csv(landmark_file_name, delimiter='\t',
                             chunksize=CHINK_SIZE)
    fnames = []

    if split in ("train", "val"):
        X = np.zeros((matrix_size[split], n_points), dtype=np.int16)
    beg_line = 0

    for chunk in tqdm.tqdm(chunks, total=matrix_size[split] // CHINK_SIZE):
        fnames.extend(chunk.iloc[:, 0].values)

        if split in ("train", "val"):
            X[beg_line:beg_line + chunk.shape[0]] = chunk.iloc[:, 1:].values

        beg_line += chunk.shape[0]

    if split in ("train", "val"):
        return fnames, X
    else:
        return fnames


def parse_arguments():
    parser = ArgumentParser(__doc__)
    parser.add_argument(
        "--data", "-d", help="Path to dir with target images & landmarks.", default=None)
    return parser.parse_args()


def main():
    args = parse_arguments()
    logging.basicConfig(level=logging.INFO)
    logging.info("convert train data")
    f_name_train, X_train = convert_data(
        os.path.join(args.data, 'train'), split="train")
    logging.info("convert val data")
    f_name_val, X_val = convert_data(
        os.path.join(args.data, 'train'), split="val")
    logging.info("convert test data")
    f_name_test = convert_data(os.path.join(args.data, 'test'), split="test")

    if not os.path.exists(os.path.join(args.data, 'prev')):
        os.makedirs(os.path.join(args.data, 'prev'))

    np.save(os.path.join(args.data, 'prev', 'X_train.npy'), X_train)
    np.save(os.path.join(args.data, 'prev', 'X_val.npy'), X_val)

    with open(os.path.join(args.data, 'prev', 'f_name_train'), "w") as f:
        for name in f_name_train:
            f.write(f"{name}\n")

    with open(os.path.join(args.data, 'prev', 'f_name_val'), "w") as f:
        for name in f_name_val:
            f.write(f"{name}\n")

    with open(os.path.join(args.data, 'prev', 'f_name_test'), "w") as f:
        for name in f_name_test:
            f.write(f"{name}\n")


if __name__ == '__main__':
    main()
